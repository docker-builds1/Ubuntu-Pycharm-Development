# Simple Docker Build


## SETUP

1. Docker Desktop must be installed. You can get it [here](https://www.docker.com/get-started).
2. If you want access to your GPU's you will need to setup nvidia-docker and get it running.

## BUILD

1. cd into the this project and run `docker build .` This will take some time to run.
2. If everything builds correctly run `docker images` to get the build hash.
3. Once you have the build hash, you can tag the image with the proper name. For example: 
`docker tag 4f1c44e5d805 darrellbest/deepspeed:latest` where 4f1c44e5d805 is the build hash
and darrellbest/deepspeed:latest is the name. Of course this name will change based on where you are
uploading it to. It is in the form of <username>/<containername>:<tag> 
4. Next you will want to push the image up to dockerhub. If you are not logged in do this now
by running `docker login` and providing your user credentials. Then run 
`docker push darrellbest/deepspeed:latest`.

## Test

1. Pull the image down from dockehub first by running `docker pull darrellbest/deepspeed:latest`.
2. Then execute `docker run -it darrellbest/deepspeed:latest /bin/bash`. This should give you a bash prompt
to test in. If you have nvidia-docker set up correctly you should be able to see the results of `nvidia-smi`.


## Cleanup

1. To clear out old build files run `docker system prune`
